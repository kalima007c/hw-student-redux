import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import InputForm from "./components/InputForm";
import {Divider} from 'antd'
import StudentList from "./components/StudentList";
import axios from 'axios'

function App() {
  return (
    <div>
      <p id="header"><h1>Redux CRUD</h1></p>
      <div className="container">
        <InputForm />
        <Divider/>
      </div>
      <div className="flex-list">
        <StudentList/>
      </div>
    </div>
  );
}

export default App;
