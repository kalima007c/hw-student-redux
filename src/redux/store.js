import { createStore, combineReducers, applyMiddleware ,compose } from "redux";
import thunk from 'redux-thunk'

// Reducer

const initialForm = {
  name: "",
  surname: "",
  major: "",
  gpa: 0
};

const formReducer = (data = initialForm, action) => {
  switch (action.type) {
    case "CHANGE_NAME":
      return { ...data, name: action.name };
    case "CHANGE_SURNAME":
      return { ...data, surname: action.surname };
    case "CHANGE_MAJOR":
      return { ...data, major: action.major };
    case "CHANGE_GPA":
      return { ...data, gpa: action.gpa };
    case "INITIAL" :
      return {
        name : '',
        surname : '',
        major : '',
        gpa : '',
      }
    case "CHANGE" : 
      return {
        ...data,
        [action.key] : action.value
      }
  }
  return data;
};

const studentReducer = (students = [], action) => {
  switch (action.type) {
    case "GET_STUDENTS":
      return action.students;
    case "ADD_STUDENTS":
      return [...students, action.student];
    case "DELETE_STUDENTS":
      return students.filter(student => +student.id !== +action.id);
    case "UPDATE_STUDENTS":
      return students.map(student => {
        if (+student.id === action.id) {
          return action.student;
        } else {
          return student;
        }
      });
  }
  return students;
};

const rootReducer = combineReducers({
  student: studentReducer,
  form: formReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer,composeEnhancers(applyMiddleware(thunk)));
