import React from "react";
import axios from "axios";
import "./StudentCard.css";
import { Button, Row, Col } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import {deleteStudent,updateStudent} from '../redux/action'
const StudentCard = props => {

  const dispatch = useDispatch();
  const form = useSelector(state => state.form);

  const act = bindActionCreators({
    deleteStudent: deleteStudent,
    updateStudent: updateStudent,
  },useDispatch());

  return (
    <div>
      <div className="card-container">
        <p>
          <h2>{props.id}</h2>
        </p>
        <p>
          <h2>{props.name + " " + props.surname}</h2>
        </p>
        <p>{"Major : " + props.major}</p>
        <p>{"GPA : " + props.gpa}</p>
        <Row>
          <Col>
            <Button onClick={()=>{act.deleteStudent(props.id)}} type="danger">
              Delete
            </Button>{" "}
            <Button onClick={()=>{act.updateStudent(props.id,form)}} type="primary">
              Update
            </Button>
          </Col>
        </Row>
      </div>
    </div>
  );
};
export default StudentCard;
