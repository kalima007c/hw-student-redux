import React, { useEffect } from 'react'
import axios from 'axios'
import StudentCard from './StudentCard'
import { useSelector, useDispatch } from 'react-redux'
import "./StudentCard.css";
const StudentList = () => {

    const students = useSelector(state => state.student)
    const dispatch  = useDispatch()
    const getStudents = async () => {
        const result = await axios.get(`http://localhost/api/students`);
        const action = {type : "GET_STUDENTS",students : result.data}
        dispatch(action)
    }
    useEffect( ()=> {
        getStudents()
    },[])

    if (!students || !students.length)
        return (<h2 id="notify">No Student</h2>)

    return (
        <div className="student-list-con">
            {
                students.map((student, index) => (
                    <div key={index} style={{ margin: 5 }}>
                        <StudentCard {...student} />
                    </div>
                ))
            }
        </div>

    )
} 

export default StudentList